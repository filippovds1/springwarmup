package com.example.springwarmup.controller;

import com.example.springwarmup.DAO.EmployeeRepository;
import com.example.springwarmup.entity.Employee;
import com.example.springwarmup.service.EmployeeService;
import org.aspectj.lang.annotation.Before;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
class MyControllerIntegrationTestWithH2DB {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private EmployeeRepository repository;

    @BeforeEach
    private void initDB(){
        repository.save(new Employee(1, "Ivan", "Ivanov", 1000));
        repository.save(new Employee(2, "Petr", "Petrov", 2000));
        repository.save(new Employee(3, "Sergey", "Sokolov", 4444));
    }
    @AfterEach
    private void clearDB(){
        repository.deleteAll();
    }

    @Test
    @DisplayName("Вызов get /employees возвращает список всех работников")
    void showAllEmployees() throws Exception {
        RequestBuilder request = MockMvcRequestBuilders
                .get("/employees")
                .accept(MediaType.APPLICATION_JSON);
        MvcResult mvcResult = mockMvc
                .perform(request)
                .andExpect(status().isOk())
                .andExpect(content().json("[{id:1, name:Ivan, surname:Ivanov, salary:1000}," +
                        "{id:2, name:Petr, surname:Petrov, salary:2000}," +
                        "{id:3, name:Sergey, surname:Sokolov, salary:4444}]"))
                .andReturn();
    }

    @Test
    @DisplayName("Вызов post /employees добавляет работника в базу")
    void addNewEmployee() throws Exception {
        RequestBuilder request = MockMvcRequestBuilders
                .post("/employees")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content("{\n" +
                        "    \"id\": \"4\",\n" +
                        "    \"name\": \"Ivan\",\n" +
                        "    \"surname\": \"Ivanov\",\n" +
                        "    \"salary\": 1000\n" +
                        "}");
        mockMvc.perform(request)
                .andExpect(status().isOk())
                .andExpect(content().json("{id:4, name:Ivan, surname:Ivanov, salary:1000}"))
                .andReturn();
    }
}