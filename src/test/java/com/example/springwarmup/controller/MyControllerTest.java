package com.example.springwarmup.controller;

import com.example.springwarmup.entity.Employee;
import com.example.springwarmup.service.EmployeeService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest
class MyControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private EmployeeService employeeService;

    @Test
    @DisplayName("Вызов get /employees возвращает список всех работников")
    void showAllEmployees() throws Exception {
        List<Employee> employees = new ArrayList<>();
        employees.add(new Employee(1, "Ivan", "Ivanov", 1000));
        employees.add(new Employee(2, "Petr", "Petrov", 2000));
        employees.add(new Employee(3, "Sergey", "Sokolov", 3000));

        when(employeeService.getAllEmployees()).thenReturn(
                employees);
        RequestBuilder request = MockMvcRequestBuilders
                .get("/employees")
                .accept(MediaType.APPLICATION_JSON);
        MvcResult mvcResult = mockMvc
                .perform(request)
                .andExpect(status().isOk())
                .andExpect(content().json("[{id:1, name:Ivan, surname:Ivanov, salary:1000}," +
                        "{id:2, name:Petr, surname:Petrov, salary:2000}," +
                        "{id:3, name:Sergey, surname:Sokolov, salary:3000}]"))
                .andReturn();
    }

    @Test
    @DisplayName("Вызов post /employees добавляет работника в базу")
    void addNewEmployee() throws Exception {
        RequestBuilder request = MockMvcRequestBuilders
                .post("/employees")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content("{\n" +
                        "    \"id\": \"1\",\n" +
                        "    \"name\": \"Ivan\",\n" +
                        "    \"surname\": \"Ivanov\",\n" +
                        "    \"salary\": 1000\n" +
                        "}");
        mockMvc.perform(request)
                .andExpect(status().isOk())
                .andExpect(content().json("{id:1, name:Ivan, surname:Ivanov, salary:1000}"))
                .andReturn();
    }
}