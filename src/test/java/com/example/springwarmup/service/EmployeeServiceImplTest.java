package com.example.springwarmup.service;

import com.example.springwarmup.DAO.EmployeeRepository;
import com.example.springwarmup.entity.Employee;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class EmployeeServiceImplTest {

    @Mock
    EmployeeRepository repository;

    @InjectMocks
    EmployeeServiceImpl service;

    @Test
    @DisplayName("Получение списка работников возвращает список")
    void getAllEmployees() {
        List<Employee> employees = new ArrayList<>();
        employees.add(new Employee(1, "Ivan", "Ivanov", 1000));
        employees.add(new Employee(2, "Petr", "Petrov", 2000));
        employees.add(new Employee(3, "Sergey", "Sokolov", 3000));

        when(repository.findAll()).thenReturn(employees);
        assertEquals(employees, service.getAllEmployees());
    }

    @Test
    @DisplayName("Сохранение работника вызывает соответствующий метод репозитория 1 раз")
    void saveEmployee() {
        Employee employee = new Employee(1, "Ivan", "Ivanov", 1000);
        service.saveEmployee(employee);
        verify(repository, times(1)).save(employee);
    }

    @Test
    @DisplayName("Получение работника по ID возвращает работника с проверкой идентичности зарплат")
    void getEmployeeByID() {
        when(repository.findById(anyInt())).thenReturn(
                Optional.of(new Employee(1, "Ivan", "Ivanov", 1000)));
        Employee employee = service.getEmployeeByID(1);
        assertEquals(1000, employee.getSalary());
    }

    @Test
    @DisplayName("Удаление работника вызывает соответствующий метод репозитория 1 раз")
    void deleteEmployeeByID() {
        Employee employee = new Employee(1, "Ivan", "Ivanov", 1000);
        service.deleteEmployeeByID(1);
        verify(repository, times(1)).deleteById(1);
    }
}