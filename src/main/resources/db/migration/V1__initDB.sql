CREATE TABLE IF NOT EXISTS employees
(
    id integer NOT NULL GENERATED ALWAYS AS IDENTITY ,
    name text NOT NULL,
    surname text NOT NULL,
    salary integer NOT NULL,
    CONSTRAINT employees_pkey PRIMARY KEY (id)
)
