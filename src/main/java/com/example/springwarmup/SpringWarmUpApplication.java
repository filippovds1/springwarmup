package com.example.springwarmup;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringWarmUpApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringWarmUpApplication.class, args);
	}

}
