package com.example.springwarmup.service;

import com.example.springwarmup.DAO.EmployeeRepository;
import com.example.springwarmup.entity.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;


@Service
public class EmployeeServiceImpl implements EmployeeService {
    @Autowired
    private EmployeeRepository employeeRepository;

    @Override
    public List<Employee> getAllEmployees() {
        return employeeRepository.findAll();
    }

    @Override
    public void saveEmployee(Employee employee) {
        employeeRepository.save(employee);
    }

    @Override
    public Employee getEmployeeByID(int id) {
        Optional<Employee> emp = employeeRepository.findById(id);
        return emp.orElseGet(null);
    }

    @Override
    public void deleteEmployeeByID(int id) {
        employeeRepository.deleteById(id);
    }

}
