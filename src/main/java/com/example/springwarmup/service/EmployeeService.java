package com.example.springwarmup.service;

import com.example.springwarmup.entity.Employee;

import java.util.List;

public interface EmployeeService {
    /**
     * Возвращает список всех работников из БД
     * @return Список работников
     */
    List<Employee> getAllEmployees();

    /**
     * Сохраняет работника в БД
     * @param employee - работник для сохранения в БД
     */
    void saveEmployee(Employee employee);

    /**
     * Возвращает работника из БД по id
     * @param id - id работника
     * @return - работник с заданным id
     */
    Employee getEmployeeByID(int id);

    /**
     * Удаляет работника из БД по id
     * @param id - id работника
     */
    void deleteEmployeeByID(int id);

}
