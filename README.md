<h1 align="center">SpringWarmUP</h1>

## Описание
**Простое приложение с использованием  Spring Boot. Выводит список работников из базы, позволяет добавлять работников в базу**

## Как использовать
1) Запустить БД PostgreSQL с помощью файла docker-compose.yaml
2) Запустить метод main в классе SpringWarmUpApplication
3) Поддерживаемые API:
-  get http://localhost:8080/employees - получение списка всех работников
- post http://localhost:8080/employees - добавление работника в БД

###### _Не для коммерческого использования_